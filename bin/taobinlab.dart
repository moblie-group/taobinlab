import 'dart:io';

String type = "";
String sel = "";
String hcs = "";
String sweet = "";
String tube = "";
String lid = "";
String pay = "";
void main(List<String> arguments) {
  SelectType();
  print("เลืออาหารประเภท $type เลือก $sel $hcs $sweet $tube $lid เลือกจ่าบแบบ $pay");
}

void SelectType() {
  print("กรุณาเลือกสินค้าประเภทเครื่องดิ่ม: ");
  print("1.เมนูแนะนำ");
  print("2.กาแฟ");
  print("3.ชา");
  print("4.นม,โกโก้ และคาราเมล");
  print("5.โปรตีนเชค โซดาและอื่นๆ");
  print("6.โซดาและอื่นๆ");
  String choose = (stdin.readLineSync()!);
  if (choose == "1") {
    type = "เมนูแนะนำ";
    select(choose);
  }else if (choose == "2") {
    type = "กาแฟ";
    select(choose);
  }else if (choose == "3") {
    type = "ชา";
    select(choose);
  }else if (choose == "4") {
    type = "นม,โกโก้ และคาราเมล";
    select(choose);
  }else if (choose == "5") {
    type = "โปรตีนเชค โซดาและอื่นๆ";
    select(choose);
  }else if (choose == "6") {
    type = "โซดาและอื่นๆ";
    select(choose);
  }
}

void select(String choose) {
  if (choose == "1") {
    print("1.เอสเพรสโซ่");
    print("2.อเมริกาโน่(กาแฟดำ)");
    print("3.สตอเบอร์รี่ปั่น");
    String choose = (stdin.readLineSync()!);
    if (choose == "1") {
      sel = "เอสเพรสโซ่";
      HotOrColdOrSmoothie();
    } else if (choose == "2") {
      sel = "อเมริกาโน่(กาแฟดำ)";
      HotOrColdOrSmoothie();
    }else if (choose == "3") {
      sel = "สตอเบอร์รี่ปั่น";
      HotOrColdOrSmoothie();
    }
  } else if (choose == "2") {
    print("1.เอสเพรสโซ่");
    print("2.มอคค่า");
    print("3.ลาเต้");
    String choose = (stdin.readLineSync()!);
    if (choose == "1") {
      sel = "เอสเพรสโซ่";
      HotOrColdOrSmoothie();
    } else if (choose == "2") {
      sel = "มอคค่า";
      HotOrColdOrSmoothie();
    }else if (choose == "3") {
      sel = "ลาเต้";
      HotOrColdOrSmoothie();
    }
  } else if (choose == "3") {
    print("1.เก๊กฮวย");
    print("2.ชาขิง");
    print("3.ชาเขียวญี่ปุ่น");
    String choose = (stdin.readLineSync()!);
    if (choose == "1") {
      sel = "เก๊กฮวย";
      HotOrColdOrSmoothie();
    } else if (choose == "2") {
      sel = "ชาขิง";
      HotOrColdOrSmoothie();
    }else if (choose == "3") {
      sel = "ชาเขียวญี่ปุ่น";
      HotOrColdOrSmoothie();
    }
  } else if (choose == "4") {
    print("1.คาราเมลโกโก้");
    print("2.นมบราวน์ชูการ์");
    print("3.นมชมพู");
    String choose = (stdin.readLineSync()!);
    if (choose == "1") {
      sel = "คาราเมลโกโก้";
      HotOrColdOrSmoothie();
    } else if (choose == "2") {
      sel = "นมบราวน์ชูการ์";
      HotOrColdOrSmoothie();
    }else if (choose == "3") {
      sel = "นมชมพู";
      HotOrColdOrSmoothie();
    }
  } else if (choose == "5") {
    print("1.สตอเบอร์รี่โปรตีน");
    print("2.เอสเพรสโซ่โปรตีน");
    print("3.คาราเมลโปรตีน");
    String choose = (stdin.readLineSync()!);
    if (choose == "1") {
      sel = "สตอเบอร์รี่โปรตีน";
      HotOrColdOrSmoothie();
    } else if (choose == "2") {
      sel = "เอสเพรสโซ่โปรตีน";
      HotOrColdOrSmoothie();
    }else if (choose == "3") {
      sel = "คาราเมลโปรตีน";
      HotOrColdOrSmoothie();
    }
  } else if (choose == "6") {
    print("1.ลิ้นจี่โซดา");
    print("2.น้ำแดงมะนาว");
    print("3.โซดา");
    String choose = (stdin.readLineSync()!);
    if (choose == "1") {
      sel = "ลิ้นจี่โซดา";
      HotOrColdOrSmoothie();
    } else if (choose == "2") {
      sel = "น้ำแดงมะนาว";
      HotOrColdOrSmoothie();
    }else if (choose == "3") {
      sel = "โซดา";
      HotOrColdOrSmoothie();
    }
  }
}

void HotOrColdOrSmoothie() {
  print("1.ร้อน");
  print("2.เย็น");
  print("3.ปั่น");
  String choose = (stdin.readLineSync()!);
  if (choose == "1") {
    hcs = "ร้อน";
    Sweetnesslevel();
  } else if (choose == "2") {
    hcs = "เย็น";
    Sweetnesslevel();
  } else if (choose == "3") {
    hcs = "ปั่น";
    Sweetnesslevel();
  }
}

void Sweetnesslevel() {
  print("1.หวานน้อย");
  print("2.หวานพอดี");
  print("3.หวานมาก");
  String choose = (stdin.readLineSync()!);
  if (choose == "1") {
    sweet = "หวานน้อย";
    gettubeornot();
  }else if (choose == "2") {
    sweet = "หวานพอดี";
    gettubeornot();
  }else if (choose == "3") {
    sweet = "หวานมาก";
    gettubeornot();
  }
}

void gettubeornot() {
  print("1.เอาหลอด");
  print("2.ไม่เอาหลอด");
  String choose = (stdin.readLineSync()!);
  if (choose == "1") {
    tube = "เอาหลอด";
    getlidornot();
  }else if (choose == "2") {
    sweet = "ไม่เอาหลอด";
    getlidornot();
  }
}

void getlidornot() {
  print("1.เอาฝา");
  print("2.ไม่เอาฝา");
  String choose = (stdin.readLineSync()!);
  if (choose == "1") {
    lid = "เอาฝา";
    payment();
  }else if (choose == "2") {
    sweet = "ไม่เอาฝา";
    payment();
  }
}

void payment() {
  print("1.เงินสด");
  print("2.แสกน QR");
  print("3.เต่าบินการ์ด");
  print("4.ใช้คูปอง");
  print("5.E-wallet");
  print("6.โปรโมชั่นอื่นๆ");
  String choose = (stdin.readLineSync()!);
  if (choose == "1") {
    pay = "เงินสด";
  }
}
